# OpenML dataset: New-York-Times-Best-Sellers

https://www.openml.org/d/43518

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
The data contains Best Sellers List published by The New York Times every Sunday. The temporal range is from 03-Jan-2010 to 29-Dec-2019 which makes it a whole decade of data. Each week, 5 books are named as best sellers for each category.
Acknowledgements

Image URL: Sincerely Media - Unsplash

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43518) of an [OpenML dataset](https://www.openml.org/d/43518). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43518/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43518/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43518/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

